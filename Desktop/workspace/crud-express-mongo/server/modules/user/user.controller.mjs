
import * as DB from "../../db/mongo_db.mjs";
// import user_model from "../user/user.model.mjs";
    
export async function createUser (req, res){
    const user = await DB.createUser(req.body);
    res.status(200).json(user);
}

export async function getAllUsers(req, res) {
    const users = await DB.getAllUsers(["_id" ,"first_name" ,"last_name" ,"email"]);
    res.status(200).json(users);

}

export async function getUserByID(req, res) {
    const user = await DB.getUserByID(req.params.id);
    if (!user) return res.status(404).json({ status: "No user found." });
    res.status(200).json(user);
}

export async function deleteUser(req, res){
    const user = await DB.deleteUserByID(req.params.id);
    if (!user) return res.status(404).json({ status: "No user found." });
    res.status(200).json(user);
}

export async function updateUser(req, res){
    const user = await DB.updateUser(req.params.id,req.body);
    res.status(200).json(user);
}

export async function paginate(req, res){
    const users = await DB.getAllUsers( ["_id" ,"first_name" ,"last_name" ,"email"], 
    req.params.page * req.params.batch_size, parseInt(req.params.batch_size));
    res.status(200).json(users);
}






